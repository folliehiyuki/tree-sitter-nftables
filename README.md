# tree-sitter-nftables

 [tree-sitter](https://github.com/tree-sitter/tree-sitter) grammar for the [scripting interface of nftables](https://wiki.nftables.org/wiki-nftables/index.php/Scripting).
